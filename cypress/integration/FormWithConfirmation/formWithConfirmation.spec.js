import {format} from "date-fns";

describe('FormWithConfirmation', () => {

    beforeEach(() => {

        cy.visit('http://localhost:3000')
    })

    it('should have today as initial value', () => {

        cy.get('input:first').should('have.value', format(new Date(), "dd MM yyyy"))
    })

    it('Date is not changed when we dont confirm changes', () => {

        cy.get('input:first').clear().type('12 12 2023')

        cy.get('[role=dialog] h2').should('be.visible')
        cy.get('[role=dialog] h2').should('have.text', 'Are you sure about the changes?')

        cy.get('[role=dialog] button').should('have.length', 2)
        cy.get('[role=dialog] button').eq(0).click()

        // Somehow the value is changed in the DOM, so this can't be used
        // cy.get('input:first').should('have.value', format(new Date(), "dd MM yyyy"))

        cy.get('.TextWithLeftPadding').should('have.text', 'Frontend technologies will be as mature as backend technologies in ' + new Date().toDateString())
    })

    it('Date is changed when we confirm changes', () => {

        cy.get('input:first').clear().type('12 12 2023', {force: true})

        cy.get('[role=dialog] button').eq(1).click()

        cy.get('.TextWithLeftPadding').should('have.text', 'Frontend technologies will be as mature as backend technologies in Tue Dec 12 2023')
    })
})
