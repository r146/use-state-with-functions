import React from "react";
import {Button, Dialog, DialogTitle} from "@mui/material";

interface ConfirmChangesModalProps {
    open: boolean
    onCancelChanges: Function
    onApplyChanges: Function
}

const ConfirmChangesModal: React.FC<ConfirmChangesModalProps> = (props) => {

    return (
        <Dialog open={props.open}>
            <DialogTitle>Are you sure about the changes?</DialogTitle>
            <Button variant="outlined" onClick={() => props.onCancelChanges()}>Discard changes</Button>
            <Button variant="contained" onClick={() => props.onApplyChanges()}>Apply changes</Button>
        </Dialog>
    )
}

export default ConfirmChangesModal
