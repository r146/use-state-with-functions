import React, {useState} from "react";
import {Box} from "@mui/material";
import ConfirmChangesModal from "./ConfirmChangesModal";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers"
import DateFnsUtils from "@date-io/date-fns";
import locale from 'date-fns/locale/en-US'

const FormWithConfirmation: React.FC = (props) => {

    const BoxStyle = {
        display: 'flex',
        flexDirection: "column",
    } as const

    const BoxItems = {
        paddingTop: "100px"
    } as const

    const [joined, setJoined] = useState<Date | null>(new Date())
    const [confirmationModalOpen, setConfirmationModalOpen] = useState(false)
    const [selectedFunction, setSelectedFunction] = useState<Function>(() => {
    })

    function displayConfirmationDialog(setNewValue: Function) {
        setConfirmationModalOpen(true)
        setSelectedFunction(() => () => {
            setNewValue()
            setConfirmationModalOpen(false)
        })
    }

    return (
        <Box sx={BoxStyle}>
            <Box sx={BoxItems}>

                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={locale}>
                    <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format={"dd MM yyyy"}
                        label="Joined on"
                        value={joined}
                        onChange={(newJoined) => {
                            newJoined && displayConfirmationDialog(() => setJoined(newJoined))
                        }}
                    />
                </MuiPickersUtilsProvider>

                <span
                    className={"TextWithLeftPadding"}>Frontend technologies will be as mature as backend technologies in {joined?.toDateString()}</span>
                <ConfirmChangesModal
                    open={confirmationModalOpen}
                    onCancelChanges={() => setConfirmationModalOpen(false)}
                    onApplyChanges={selectedFunction}/>
            </Box>
        </Box>

    )
}

export default FormWithConfirmation
